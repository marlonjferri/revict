<?php

include_once $_SERVER['DOCUMENT_ROOT'].'/core/models/Model.php';

class Debtor extends Model
{
    protected $modelTable = 'debtors';


    public function __construct()
    {
        parent::__construct();
    }


    public function debits()
    {
        return $this->hasMany('debits','Debit');
    }
}