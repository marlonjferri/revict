<?php

    include_once $_SERVER['DOCUMENT_ROOT'].'/core/models/Model.php';

class Debit extends Model
{
    protected $modelTable = 'debits';


    public function __construct()
    {
        parent::__construct();
    }


    public function debtor()
    {
        return $this->belongsToOne('debtors','Debtor');
    }
}