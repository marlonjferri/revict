<?php

include_once $_SERVER['DOCUMENT_ROOT'].'/core/models/Table.php';

class DebtorTable extends Table
{
    public function __construct($table = 'debtors')
    {
        parent::__construct($table);
    }


    public function create()
    {
        $this->createSql = "CREATE TABLE `".$this->table."` (
        `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
        `name` VARCHAR(255) NULL,
        `cpf_cnpj` INT NULL,
        `birthdate` DATE NULL,
        `address` VARCHAR(255) NULL,
        `created_at` DATETIME NULL,
        `updated_at` DATETIME NULL,
        `deleted_at` DATETIME NULL,
        PRIMARY KEY (`id`));";

        parent::create();
    }





}