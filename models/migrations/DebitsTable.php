<?php

include_once $_SERVER['DOCUMENT_ROOT'].'/core/models/Table.php';

class DebitsTable extends Table
{
    public function __construct($table = 'debits')
    {
        parent::__construct($table);
    }


    public function create()
    {
        $this->createSql = "CREATE TABLE `".$this->table."` (
            `id` INT NOT NULL AUTO_INCREMENT,
            `debtor_id` INT NOT NULL,
            `description` TEXT NULL,
            `value` FLOAT NULL,
            `due_at` DATE NULL,
            `created_at` DATETIME NULL,
            `updated_at` DATETIME NULL,
            `deleted_at` DATETIME NULL,
            PRIMARY KEY (`id`),
            INDEX `fk_debts_1_idx` (`debtor_id` ASC),
            CONSTRAINT `fk_debts_1`
            FOREIGN KEY (`debtor_id`)
            REFERENCES `marlo682_revict`.`debtors` (`id`)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION);";

        parent::create();
    }









}