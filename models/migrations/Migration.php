<?php


class Migration
{

    private $tables = [
        'DebtorTable',
        'DebitsTable'
    ];

    public function doMigration()
    {
        foreach ($this->tables as $table){
            include $_SERVER['DOCUMENT_ROOT'].'/models/migrations/'.$table.'.php';
            $table = new $table();
            $table->create();
        }

    }
}