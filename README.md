# Revict

 Teste Revict
 
 
 Seguir as instruções:
 Esta parte é imprescindível.
 
 Utilize o git e faça commits de forma que possamos analisar como foi desenvolvida a solução por alguma plataforma como Github ou Bitbucket. Ao final, me envie a URL do repositório no Github ou Bitbucket.
 
 Missão
 
 Desenvolver um CRUD em PHP puro (orientado a objetos e sem utilizar framework) atendendo à especificação abaixo.
 
 Especificação:
 
 Monte uma base de cadastro de devedores e dívidas com, no mínimo, os campos abaixo (modelagem de dados importa):
 Nome: string
 CPF/CNPJ: integer
 Data de nascimento: date
 Endereço: string
 Descrição do título: text
 valor: float
 Data de vencimento: date
 updated: datetime
 Utilize MySQL.
 
 A aplicação deve ser cross browser support (chrome, firefox, etc)
 
 Dashboard com indicadores que julgue apropriados (Bônus star)
 
 Escrever teste unitário (Bônus star)
 
 Dica
 Não pode ser utilizado framework PHP, porém para o front podem ser utilizadas ferramentas como bootstrap.
 
 
 <hr>
 <h3>Configurações</h3>
 Para configuração do banco de dados utilizar o arquivo config/databaseConfig.php.   