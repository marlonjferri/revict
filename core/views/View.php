<?php


interface View
{
    public function render($page, $values);

}