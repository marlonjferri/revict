<?php


class SoftDeletesScope
{

    public static function addSoftDeleteWhereSelect()
    {
        return 'deleted_at is not null';
    }

    public static function addSoftDeleteWhereUpdate()
    {
        return 'deleted_at is not null';
    }

    public static function addOnlyTrashed()
    {

    }


}