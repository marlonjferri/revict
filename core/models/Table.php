<?php


class Table
{

    protected $table;
    protected $createSql;

    /**
     * Table constructor.
     * @param $table
     */
    public function __construct($table = '')
    {
        $this->table = $table;
//        $this->create();
    }


    public function create(){

        $this->doCreate();
    }

    protected function doCreate(){
        global $DB;

        $DB->execute($this->createSql) ;
    }



}