<?php
//include_once ('SoftDeletesScope.php');


trait SoftDeletes
{

    protected $isSoftDeletes = true;



    public function _onlyTrashed()
    {
//        echo "<br>_onlyTrashed<br>";
        $this->isSoftDeletes = false;
        $this->_whereIsNotNull('deleted_at');
//        echo "<br>_onlyTrashed ".$this->isSoftDeletes."<br>";
        return $this;
    }
    public function _withTrashed()
    {
//        echo "<br>_withTrashed<br>";
        $this->isSoftDeletes = false;
//        echo "<br>_withTrashed ".$this->isSoftDeletes."<br>";
        return $this;
    }
    public function _withoutTrashed()
    {
        $this->isSoftDeletes = true;
        $this->_whereIsNull('deleted_at');
        return $this;
    }
    public function _restore()
    {
        $this->isSoftDeletes = false;
        return $this->_update([
            'deleted_at' => null
        ]);

    }

}