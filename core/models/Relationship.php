<?php

class Relationship
{

    public function getIfInRelationship($relationship, $property)
    {
        $instanceName = get_called_class();
        $tmpObj = new $instanceName();

        if(!method_exists($tmpObj,$relationship))
            return null;
        $tmpObj->id=0;

        $tmpObj = $tmpObj->$relationship();

        $property = 'get' . ucfirst($property);

        return $tmpObj->$property();
    }
}