<?php


trait QueryBuilder
{
    private $DB;

    public $methodPrefix = '_';

    protected $relatedToQueryModel = null;
    protected $relatedToQueryId = null;
    protected $relatedToQueryMyColumn = null;
    protected $relatedToQueryColumn = null;

    protected $select = '';
    protected $selectRaw = '';
    protected $from = '';
    protected $where = '';
    protected $join = '';
    protected $orderBy = '';
    protected $groupBy = '';
    protected $limit = '';
    protected $offset = '';
    protected $raw ='';

//    protected $relationship = '';
//
//    protected $toObjetifyClass = '';
//    private $returnType = '';


    //--------------------------------------- DB METHODS ---------------------------------------

    protected function execute()
    {
        global $DB;


        $this->checkSoftDeletes();

        if($this->raw === '') {

            $sql = 'SELECT ';
            if ($this->select !== '')
                $sql .= $this->select;
            else
                $sql .= '*';

            if ($this->selectRaw !== '')
                $sql .= ', ' . $this->selectRaw;

            $sql .= ' FROM ';
            if($this->from == '')
                $sql .= $this->modelTable;
            else
                $sql .= $this->from;

            if ($this->join !== '') {
                $sql .= $this->join;
            }

            if ($this->where !== '') {
                $sql .= ' WHERE ';
                $sql .= $this->where;
            }

            if ($this->groupBy !== '') {
                $sql .= ' GROUP BY ';
                $sql .= $this->groupBy;
            }
            if ($this->orderBy !== '') {
                $sql .= ' ORDER BY ';
                $sql .= $this->orderBy;
            }

            if ($this->limit !== '') {
                $sql .= ' LIMIT ';
                $sql .= $this->limit;
            }
        }else
            $sql = $this->raw;


//        echo $sql . "<br>";
        $this->clean();

        $ret = $DB->execute($sql);

//        print_r($ret);
        return $ret;

    }

    public function getSql()
    {

        $this->checkSoftDeletes();

        if($this->raw === '') {

            $sql = 'SELECT ';
            if ($this->select !== '')
                $sql .= $this->select;
            else
                $sql .= '*';

            if ($this->selectRaw !== '')
                $sql .= ', ' . $this->selectRaw;


            $sql .= ' FROM ';
            if($this->from == '')
                $sql .= $this->modelTable;
            else
                $sql .= $this->from;

            if ($this->join !== '') {
                $sql .= $this->join;
            }

            if ($this->where !== '') {
                $sql .= ' WHERE ';
                $sql .= $this->where;
            }

            if ($this->groupBy !== '') {
                $sql .= ' GROUP BY ';
                $sql .= $this->groupBy;
            }
            if ($this->orderBy !== '') {
                $sql .= ' ORDER BY ';
                $sql .= $this->orderBy;
            }

            if ($this->limit !== '') {
                $sql .= ' LIMIT ';
                $sql .= $this->limit;
            }
        }else
            $sql = $this->raw;


        return $sql;

    }

    protected function _create($data)
    {
        if(!isset($this))
            return static::runStatic(__FUNCTION__,func_get_args());


        if(!$this->checkRelationship(__FUNCTION__))
            return null;
        try {
            if (!$this->isAssoc($data))
//                foreach ($data as $item)
//                    $this->newCreate($item);
                $data = array_map(array($this, 'newCreate'),$data );
            else
                $data = $this->newCreate($data);


        }catch (Error $error){
            echo $error->getMessage();
            $this->clean();
            return false;
        }

        $this->clean();
        return $data;
    }

    protected function newCreate($data)
    {
        global $DB;


        $sql = 'INSERT INTO ';

        if ($this->from == '')
            $sql .= $this->modelTable;
        else
            $sql .= $this->from;


        //-------------------------set keys
        $sql .= ' (';

        foreach (array_keys($data) as $key) {
            if (array_keys($data)[0] !== $key)
                $sql .= ', ';
            $sql .= $key;
        }


        if (count($data) > 0)
            $sql .= ', ';


        //-------------------------set default keys
        if(!empty($this->relatedToQueryModel) && !empty($this->relatedToQueryId))
            $sql .= ' ' . $this->relatedToQueryModel . ', ';

        $sql .= ' created_at, updated_at';

        $sql .= ')';

        $sql .= ' VALUES (';

        foreach (array_keys($data) as $key) {
            if (array_keys($data)[0] !== $key)
                $sql .= ', ';

            if(is_null($data[$key]))
                $sql .= 'NULL';
            elseif (is_numeric($data[$key]))
                $sql .=  $data[$key] ;
            else
            $sql .= '\'' . $data[$key] . '\'';
        }

        if (count($data) > 0)
            $sql .= ', ';

        if($this->relatedToQueryModel && $this->relatedToQueryId)
            $sql .= $this->relatedToQueryId . ', ';

        $sql .= '\'' . date('Y-m-d H:i:s') . '\', \'' . date('Y-m-d H:i:s') .'\'' ;

        $sql .= ')';

//        echo"<br>$sql<br>";

        $to = $DB->execute($sql);
        $to = $this->find($DB->inserted());
        $this->relationship = '';
        return $to;


    }

    public function _update($data)
    {
        global $DB;

        if (empty($this->where) && !isset($this->id))
            $this->_where('id','=',$this->id);


        $this->checkSoftDeletes();

        try {

            $sql = ' UPDATE ';

            if ($this->from == '')
                $sql .= $this->modelTable;
            else
                $sql .= $this->from;

            $sql .= ' SET ';

            foreach (array_keys($data) as $property) {
                if (array_keys($data)[0] !== $property)
                    $sql .= ', ';


                if($property === 'updated_at')
                    $data[$property] = date('Y-m-d H:i:s');

                if(is_null($data[$property]))
                    $data[$property] = 'NULL';
                elseif (!is_numeric($data[$property]) && is_string($data[$property]))
                    $data[$property] = '\'' . $data[$property] . '\'';


                $sql .= $property . ' = ' . $data[$property];
            }


            if ($this->where !== '') {
                $sql .= ' WHERE ';
                $sql .= $this->where;
            }


//            echo $sql . "<br>";


            $DB->execute($sql);
        }catch (Exception $exception){
            echo $exception->getMessage();
            $this->clean();
            return false;
        }


        $this->clean();
        return $this;

    }

    public function _delete()
    {
        global $DB;

        try {
            if (!empty($this->isSoftDeletes)) {
                if ($this->where === "")
                    $this->_where('id', '=', $this->id);
                return $this->update(['deleted_at' => date('Y-m-d H:i:s')]);
            }


            $sql = ' DELETE FROM ';

            if ($this->from == '')
                $sql .= $this->modelTable;
            else
                $sql .= $this->from;


            if ($this->where !== '') {
                $sql .= ' WHERE ';
                $sql .= $this->where;
            } else
                $sql .= ' WHERE id = ' . $this->id;

//            echo $sql . "<br>";
            $this->clean();

            $DB->execute($sql);
        }catch (Exception $exception){
            echo $exception->getMessage();
            $this->clean();
            return false;
        }


        $this->clean();
        return true;

    }

    //--------------------------------------- DB METHODS ---------------------------------------



    //--------------------------------------- RAW METHODS ---------------------------------------

    public function _selectRaw($query)
    {
        if(!empty($query))
            $this->setSelectRaw($query);
        return $this;
    }

    public function _whereRaw($query)
    {
        if(!empty($query))
            $this->setWhere($query);
        return $this;
    }

    public function _orderByRaw($query)
    {
        if(!empty($query))
            $this->setOrderBy($query);
        return $this;
    }

    public function _groupByRaw($query)
    {
        if(!empty($query))
            $this->setGroupBy($query);
        return $this;
    }


    public static function sqlRaw($query)
    {
        global  $DB;
        return $DB->execute($query);
    }
    //--------------------------------------- RAW METHODS ---------------------------------------


    //--------------------------------------- TRANSICTION METHODS ---------------------------------------
    private function beginTransiction()
    {
        global $DB;
        $DB->execute('beginTransiction');
    }

    private function rollbackTransiction()
    {
        global $DB;
        $DB->execute('rollback');
    }

    private function commitTransiction()
    {
        global $DB;
        $DB->execute('commit');
    }
    //--------------------------------------- TRANSICTION METHODS ---------------------------------------





    //-------------------------------------Builders
    /**
     * @param array $list
     * @return $this
     */
    public function _select(array $list)
    {
        foreach ($list as $item)
            $this->setSelect($item);

        return $this;
    }

    /**
     * @param $firstColumn
     * @param null $operator
     * @param null $value
     * @return $this
     */
    public function _where($firstColumn, $operator = null, $value = null){
        $sqlWhere = '';

        if(!is_numeric($value ) && is_string($value))
            $value = '\''.$value.'\'';

        if(is_array($firstColumn)){

            foreach ($firstColumn as $key=>$clause) {
                if($key != 0 && $sqlWhere !== '')
                    $sqlWhere .= " AND ";
                if (!is_numeric($clause[2]) && is_string($clause[2]))
                    $clause[2] = '\'' . $clause[2] . '\'';

                $sqlWhere .= ' ' . $clause[0] . ' ' . $clause[1] . ' ' . $clause[2];
            }
        }else
            $sqlWhere .= ' ' . $firstColumn . ' ' . $operator . ' ' . $value;


        $this->setWhere($sqlWhere);

        return $this;
    }

    public function _whereIsNotNull($column)
    {
        if(is_array($column)){
            foreach ($column as $singleColumn)
                $this->setWhere($singleColumn . " IS NOT NULL");
        }
        else
            $this->setWhere($column . " IS NOT NULL");

        return $this;
    }
    public function _whereIsNull($column)
    {
        if(is_array($column)){
            foreach ($column as $singleColumn)
                $this->setWhere($singleColumn . " IS NULL");
        }
        else
            $this->setWhere($column . " IS NULL");

        return $this;
    }


    /**
     * @param $column
     * @param $order
     * @return $this
     */
    public function _orderBy($column, $order = 'asc')
    {

        if(isset($order))
            $this->setOrderBy($column . ' ' . $order);
        else
            $this->setOrderBy($column);
        return $this;
    }

    /**
     * @param $limit
     * @return $this
     */
    public function _limit($limit)
    {
        $this->setLimit($limit);

        return $this;
    }

    public function _offset($offset)
    {
        $this->setOffset($offset);

        return $this;
    }

    /**
     * @param $column
     * @return $this
     */
    public function _groupBy($column)
    {
        $this->setGroupBy($column);

        return $this;
    }

    public function _innerJoin($table, $key, $operator, $reference)
    {
        $sql = ' INNER JOIN '.$table;
        $sql .= ' ON ' . $key .  ' ' .  $operator . ' ' . $reference;
        $this->setJoin($sql);



        return $this;
    }

    public function _leftJoin($table, $key, $operator, $reference)
    {
        $sql = ' LEFT JOIN '.$table;
        $sql .= ' ON ' . $key .  ' ' .  $operator . ' ' . $reference;
        $this->setJoin($sql);



        return $this;
    }

    public function _join($table, $key, $operator, $reference)
    {

        return $this;
    }

    public function _rightJoin($payload)
    {
        return $this;
    }




    //-------------------------------------Setters

    /**
     * @param string $select
     */
    public function setSelect($select)
    {
        if($this->select == '')
            $this->select = $select;
        else
            $this->select .= ', ' . $select;
    }
    public function setSelectRaw($selectRaw)
    {
        if($this->selectRaw == '')
            $this->selectRaw = $selectRaw;
        else
            $this->selectRaw .= ', ' . $selectRaw;
    }

    /**
     * @param string $from
     */
    public function setFrom($from)
    {
        if($this->from == '')
            $this->from = $from;
        else
            $this->from .= ', ' . $from;
    }

    /**
     * @param string $where
     */
    public function setWhere($where)
    {
        if($this->where == '')
            $this->where = $where;
        else
            $this->where .= ' AND ' . $where;
    }

    /**
     * @param string $join
     */
    public function setJoin($join)
    {
        if($this->join == '')
            $this->join = $join;
        else
            $this->join .= ' ' . $join;
    }

    /**
     * @param string $orderBy
     */
    public function setOrderBy($orderBy)
    {
        if($this->orderBy == '')
            $this->orderBy = $orderBy;
        else
            $this->orderBy .= ', ' . $orderBy;
    }

    /**
     * @param string $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    /**
     * @param string $offset
     */
    public function setOffset($offset)
    {
        $this->offset = $offset;
    }

    /**
     * @param string $groupBy
     */
    public function setGroupBy($groupBy)
    {
        if($this->groupBy == '')
            $this->groupBy = $groupBy;
        else
            $this->groupBy .= ' and ' . $groupBy;
    }

    /**
     * @param string $raw
     */
    public function setRaw($raw)
    {
        $this->raw = $raw;
    }





    //---------------------------------------Getters
    /**
     * @return mixed
     */
    public function getRelatedToQueryModel()
    {
        return $this->relatedToQueryModel;
    }

    /**
     * @return mixed
     */
    public function getRelatedToQueryId()
    {
        return $this->relatedToQueryId;
    }

    /**
     * @return mixed
     */
    public function getRelatedToQueryColumn()
    {
        return $this->relatedToQueryColumn;
    }

    /**
     * @return null
     */
    public function getRelatedToQueryMyColumn()
    {
        return $this->relatedToQueryMyColumn;
    }





    //---------------------------------------Aux
    private function checkSoftDeletes()
    {
        if(!empty($this->isSoftDeletes))
            $this->_withoutTrashed();

        return !empty($this->isSoftDeletes);

    }



    private function clean()
    {
        $this->select = '';
        $this->from = '';
        $this->where = '';
        $this->join = '';
        $this->orderBy = '';
        $this->groupBy = '';
        $this->limit = '';
        $this->raw ='';

    }

    protected function checkRelationship($method)
    {
        return true;

        if($this->relationship == '')
            return true;

        $arr = [
            'belongsToOne'  => ['update','create'],
            'belongsTo'     => ['update'],
            'hasMany'       => ['update','create'],
            'hasOne'        => ['update','create']
        ];

        return in_array($method,$arr[$this->relationship]);
    }

    /**
     * @return bool
     */
    protected function checkToStatic(){
       return !isset($this);
    }

    protected static function runStatic($name,$args)
    {

        $obj = self::getNewSelfInstance();
        return $obj->$name(...$args[0]);
    }

//
//    /**
//     * Check if an array is an associative array and no numeric sequential
//     *
//     * @param array $arr
//     * @return bool
//     */
//    private function isAssoc(array $arr)
//    {
//        if (array() === $arr) return false;
//        return array_keys($arr) !== range(0, count($arr) - 1);
//    }
}