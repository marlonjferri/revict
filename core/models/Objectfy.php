<?php


trait Objectfy
{
    protected $targetModel = null;


    /**
     * Conver an array to an given class object. If it's a numeric sequencial array, return an array of given class type
     *
     * @param $data
     * @param null $model
     * @return array
     */
    protected function toClass($data, $model = null)
    {
        if($data == null || count($data) == 0)
            return $data;

        if($model == null)
            $model = empty($this->targetModel)? get_called_class():$this->targetModel;

        $this->dynamicClassImport($model);

        if($this->isAssoc($data)) {

            $obj = new $model();
            foreach (array_keys($data) as $property) {
                $obj->$property = $data[$property];
            }
            return $obj;
        }else
            return array_map(array($this,'toClass'),$data);


    }


    private function dynamicClassImport($model,$path = 'classes/models/')
    {
        if(!empty($model) && !class_exists($model))
            inc1Site($path.$model . '.php');
    }


    /**
     * Check if an array is an associative array and no numeric sequential
     *
     * @param array $arr
     * @return bool
     */
    private function isAssoc(array $arr)
    {
        if (array() === $arr) return false;
        return array_keys($arr) !== range(0, count($arr) - 1);
    }


}