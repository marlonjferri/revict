<?php
inc1Common('classes/models/Relationship.php');
//include('Relationship.php');
include('QueryBuilder.php');
include('Objectfy.php');

class Model extends Relationship
{
    use QueryBuilder;
    use Objectfy;
    

    protected $cfg;

    protected $modelTable;

    protected $stdObj = null;

    protected $relationship = '';


    protected $returnType = null;


    /**
     * Model constructor.
     */
    public function __construct()
    {
        global $DB;
        global $cfg;

        $this->DB = $DB;
        $this->cfg = $cfg;

        $this->stdObj = new stdClass();

//        $this->targetModel = $this->modelTable;


//        $this->getNewSelfInstance();
//        $this->setFrom($this->modelTable);
    }



    //--------------------------------------- RELATIONSHIPS ---------------------------------------
    /**
     * Point this object as a holder of id_other_table column
     *
     * @param $table table in sql database linked to
     * @param $model reference the model class
     * @param $foreignKey refernces the column in the referenced model. Specify if you don't use default id_[table name] notation
     * @param $referencedKey Specify if you don't refer to id column
     *stdObj
     * @return $this
     */
    protected function belongsToOne($table, $model, $foreignKey = null, $referencedKey = null)
    {
        $this->relationshipImport($model);
        $this->returnType = 'object';
        $this->relationship = __FUNCTION__;
        $this->targetModel = $model;


        $foreignKey = empty($foreignKey) ? 'id_'.$table : $foreignKey;
        $referencedKey = empty($referencedKey) ? 'id' : $referencedKey;

        $this->relatedToQueryModel = $model;
        $this->relatedToQueryId = $this->$foreignKey;
        $this->relatedToQueryMyColumn = $foreignKey;
        $this->relatedToQueryColumn = $referencedKey;

        $this->setFrom($table);

        if($referencedKey)
            $sql = $referencedKey . ' = ';
        else
            $sql = ' id  = ';

        if($foreignKey)
            $sql .= $this->stdObj->$foreignKey;
        else {
            $foreignKey = 'id_' . $table;
            $sql .= $this->stdObj->$foreignKey;
        }

        $this->setWhere($sql);

        $this->setLimit(1);

        return $this->setObjectResponse();
    }

    protected function belongsTo($table, $model, $foreignKey = null, $referencedKey = null)
    {
        $this->relationshipImport($model);
        $this->relationship = __FUNCTION__;
        $this->targetModel = $model;


        $foreignKey = empty($foreignKey) ? 'id_'.$table : $foreignKey;
        $referencedKey = empty($referencedKey) ? 'id' : $referencedKey;

        $this->relatedToQueryModel = $model;
        $this->relatedToQueryId = $this->$foreignKey;
        $this->relatedToQueryMyColumn = $foreignKey;
        $this->relatedToQueryColumn = $referencedKey;


        $this->setFrom($table);

        if($referencedKey)
            $sql = $referencedKey . ' = ';
        else
            $sql = ' id  = ';

        if($foreignKey)
            $sql .= $this->stdObj->$foreignKey;
        else {
            $foreignKey = 'id_' . $table;
            $sql .= $this->stdObj->$foreignKey;
        }

        $this->setWhere($sql);

        $this->setLimit(1);

        return $this->setObjectResponse();
    }

    protected function belongsToMany()
    {
    }

    protected function hasOne()
    {
    }

    protected function hasMany($table, $model, $foreignKey = null, $referencedKey = null)
    {
        $this->relationshipImport($model);

        $this->relationship = __FUNCTION__;
        $this->targetModel = $model;


        $referencedKey = empty($referencedKey) ? 'id_'.$this->modelTable : $referencedKey;
        $foreignKey = empty($foreignKey) ? 'id' : $foreignKey;

        $this->relatedToQueryModel = $model;
        $this->relatedToQueryId = $this->$foreignKey;
        $this->relatedToQueryMyColumn = $foreignKey;
        $this->relatedToQueryColumn = $referencedKey;



        $this->setFrom($table);

        if($referencedKey)
            $sql = $referencedKey . ' = ';
        else
            $sql = ' id_'.$this->modelTable.'  = ';

        if($foreignKey)
            $sql .= $this->stdObj->$foreignKey;
        else
            $sql .= $this->stdObj->id;




        $this->setWhere($sql);


        return $this->setArrayResponse();
    }

    //--------------------------------------- RELATIONSHIPS ---------------------------------------



    //--------------------------------------- NOT RELATIONSHIPS FUNCTIONS ---------------------------------------
    public function get()
    {
        return $this->processReturn($this->toClass($this->execute()));
    }

    public function first()
    {
        $ret = $this->execute();
        return $this->toClass(isset($ret[0])?$ret[0]:$ret,$this->targetModel);
    }

    public function last()
    {
        return $this->toClass(array_pop($this->execute()),$this->targetModel);
    }

    public function save()
    {

        if(!empty($this->stdObj->id)) {
            return $this->where('id','=',$this->stdObj->id)->_update(get_object_vars($this->stdObj));
        }else {
            $obj = $this->_create(get_object_vars($this->stdObj));
            if($obj) {
                $this->id = $obj->id;
                return $this;
            }else
                return false;
        }
    }
    //---------- staics
    public static function find($id)
    {
        $obj = self::getNewSelfInstance();

        return $obj->setObjectResponse()->where('id','=',$id)->limit(1)->get();

//        $rec = $obj->setObjectResponse()->where('id','=',$id)->limit(1)->get();
//        return empty($rec[0]) ? null: $rec[0];
    }

    public function _all()
    {
        return $this->get();
//        return self::getNewSelfInstance()->get();
    }
    //---------- staics



    public function paginate($numberItens)
    {
        $page = !empty($_REQUEST['page'])?$_REQUEST['page']:1;
        $this->limit($numberItens)->offset($numberItens*($page-1));
        $this->returnType = 'array';
//        print_r($this->get());
        return $this->get();

    }

    //--------------------------------------- NOT RELATIONSHIPS FUNCTIONS ---------------------------------------



    //--------------------------------------- AUX RELATIONSHIP FUNCTIONS ---------------------------------------
    public function with()
    {}

    public function count()
    {

    }

    public function _withCount(...$columns)
    {
        foreach($columns as $column){

            if(method_exists($this,$column)) {

                $this->_selectRaw('ifnull(temp_' . $column . '.counted,0) as count_' . $column);

//                $this->_innerJoin(
//                    '(SELECT ' . $this->getIfInRelationship($column,'relatedToQueryColumn'). ' AS temp_id_' . $column . ', SUM(1) AS counted
//                    FROM ' . $column . ' GROUP BY temp_id_' . $column . ') AS temp_' . $column,
//                    'temp_' . $column,
//                    '=',
//                    $this->getIfInRelationship($column,'modelTable'). '.' . $this->getIfInRelationship($column,'relatedToQueryId')
//                );



                $this->_leftJoin(
                    '(SELECT ' . $this->getIfInRelationship($column,'relatedToQueryColumn'). ' AS temp_id_' . $column . ', SUM(1) AS counted 
                    FROM ' . $column . ' GROUP BY temp_id_' . $column . ') AS temp_' . $column,
                    'temp_' . $column.'.temp_id_'.$column,
                    '=',
                    $this->modelTable. '.' . $this->getIfInRelationship($column,'relatedToQueryMyColumn')
                );
            }
        }

        return $this;

    }
    //--------------------------------------- AUX RELATIONSHIP FUNCTIONS ---------------------------------------


    //--------------------------------------- MAGIC METHODS ---------------------------------------
    public function __get($name)
    {
        if(method_exists($this,$name)){
            return $this->$name()->get();
        }else{
            if(property_exists($this->stdObj, $name))
                return $this->stdObj->$name;
            else
                return null;
        }
    }

    public function __set($name, $value)
    {
        $this->stdObj->$name = $value;
    }


    public function __call($name, $arguments)
    {
        if(method_exists($this,$name))
            return $this->$name(...$arguments);
        elseif (method_exists($this,$this->methodPrefix.$name)){
            $name = $this->methodPrefix.$name;
            return $this->$name(...$arguments);
        }else
            return null;
    }


    public static function __callStatic($name, $arguments)
    {
        $obj = self::getNewSelfInstance();
        if(method_exists($obj, $name))
            return $obj->$name(...$arguments);
        elseif (method_exists($obj,$obj->methodPrefix.$name)){
            $name = $obj->methodPrefix.$name;
            return $obj->$name(...$arguments);
        }else
            return null;
    }

    public function __isset($name)
    {
        return property_exists($this->stdObj, $name) || method_exists($this,$name);
    }
    //--------------------------------------- MAGIC METHODS ---------------------------------------





    //--------------------------------------- RETURN FUNCTIONS ---------------------------------------
    private function processReturn($data, $returnType = null)
    {
        if($returnType == null)
            $returnType = $this->returnType;

        if($returnType == 'object' && is_array($data) && count($data) == 1)
            return $data[0];
        elseif ($returnType == 'object' && is_array($data) && count($data) > 1)
            return $data[0];
        elseif ($returnType == 'object' && is_object($data))
            return $data;
        elseif($returnType == 'array' && is_object($data))
            return array($data);
        elseif($returnType == 'array' && empty($data))
            return array();
        else
            return $data;

    }
    public function setObjectResponse()
    {
        $this->returnType = 'object';
        return $this;
    }
    public function setArrayResponse()
    {
        $this->returnType = 'array';
        return $this;
    }
    //--------------------------------------- RETURN FUNCTIONS ---------------------------------------


    //--------------------------------------- AUX METHODS ---------------------------------------


    protected static function getNewSelfInstance()
    {
        $objClass = get_called_class();
        return new $objClass();

    }


    public function toJson()
    {
        $arr = array();

        foreach (array_keys((array)$this->stdObj) as $property) {
            $arr[$property] = !is_string($this->$property) ? $this->$property : utf8_encode($this->$property);
        }

        return json_encode($arr);
    }
}