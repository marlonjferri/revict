<?php


class Database
{
    private $conn;

    /**
     * Database constructor.
     */
    public function __construct()
    {
        global $DB;



        if(empty($DB))
            $this->connect();

        $DB = $this;
    }

    private function connect()
    {
        include_once $_SERVER['DOCUMENT_ROOT'].'/config/databaseConfig.php';

        // Create connection
        $this->conn = new mysqli(SERVER_NAME, USER_NAME, PASSWORD);
    }

    public function execute($sql)
    {
        $arr = array();
        $result = mysqli_query($this->conn, $sql) ;//or $this->_errorHandle($sql);
        if(is_bool($result)){
            return $result;
        } else {
            if(mysqli_num_rows($result) > 0){
                while ($item = mysqli_fetch_assoc($result)){
                     $arr[] = $item;
                }
            }
            mysqli_free_result($result);
            return $arr;
        }


    }

    public function one($sql){
        $arr = array();

        $result = mysqli_query($this->conn, $sql) or $this->_errorHandle($sql);
        if(is_bool($result)){
            return $result;
        } else {
            if(mysqli_num_rows($result) > 0){
                $arr = mysqli_fetch_assoc($result);
            }
            mysqli_free_result($result);
            return $arr;
        }
    }

    public function inserted(){
        return mysqli_insert_id($this->conn);
    }
}