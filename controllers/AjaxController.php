<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/core/db/Database.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/models/migrations/Migration.php';

class AjaxController
{
    private $action;

    public function __construct()
    {
        new Database();
    }


    public function setAction($action)
    {
        $this->action = $action;
        $this->route();
    }

    public function route()
    {
        switch ($this->action){
            case 'migrate':
                $migration = new Migration();
                echo ($migration->doMigration())?'Tabelas criadas com sucesso.':'Falha ao criar tabelas';
                break;
            case 'seed':

                break;
            case 'addDebtor':

                break;
            default:
                echo 'Error';
                break;
        }

    }
}