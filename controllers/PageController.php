<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/core/db/Database.php';
include_once $_SERVER['DOCUMENT_ROOT'].'/views/PageView.php';


class PageController
{
    private $page;

    public function __construct()
    {
        new Database();
    }


    public function setPage($page)
    {
        $this->page = $page;
        $this->route();
    }

    public function route()
    {
        switch ($this->page){
            case 'dashboard':
                $this->dashboardShow();
                break;
            case 'list':
                $this->listShow();
                break;
            case 'database':
                $this->databaseShow();
                break;
            default:
                $this->notFound();
                break;
        }

    }



    private function dashboardShow()
    {
        $this->view('dashboard.php',[]);
    }


    private function listShow()
    {
        $this->view('list.php',[]);
    }


    private function databaseShow()
    {
        $this->view('database.php',[]);
    }


    private function notFound()
    {
        $this->view('404.php',[]);
    }



    private function view($page, $values)
    {
        $pageView = new PageView();
        $pageView->render($page,$values);
    }



}