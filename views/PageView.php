<?php

include_once $_SERVER['DOCUMENT_ROOT'].'/core/views/View.php';

class PageView implements View
{

    public function render($page, $values)
    {
        foreach ($values as $key=>$value)
            $$key = $value;

        include $_SERVER['DOCUMENT_ROOT'].'/views/'.$page;
    }
}