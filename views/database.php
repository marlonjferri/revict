<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Revict</title>

    <script src="/includes/jquery-3.4.1.min.js"></script>

    <style>
        button{
            float: left;
            width: 100px;
            margin-left: 15px;
        }

        div{
            width: 100%;
            display: block;

        }
    </style>
</head>
<body>
    <button id="migrations">Iniciar banco</button>
    <button id="seeds">Alimentar banco</button>
    <div id="return-conteiner"></div>


    <script type="application/javascript">
        $(document).ready(function(){

            $('#migrations').on('click',()=>{
                $.ajax({
                    url: "/ajax.php?action=migrate",
                    method: 'post',
                    success: function( result ) {
                        $( "#return-container" ).html( "<strong>" + result + "</strong>" );
                    }
                });
            })

            $('#seeds').on('click',()=>{
                $.ajax({
                    url: "/ajax.php?action=seed",
                    method: 'post',
                    success: function( result ) {
                        $( "#return-container" ).html( "<strong>" + result + "</strong>" );
                    }
                });
            })

        });
    </script>
</body>
</html>